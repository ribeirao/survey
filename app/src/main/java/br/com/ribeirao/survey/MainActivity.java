package br.com.ribeirao.survey;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ribeirao.survey.model.Answer;
import br.com.ribeirao.survey.model.Question;
import br.com.ribeirao.survey.model.QuestionAnswer;
import br.com.ribeirao.survey.model.Survey;
import br.com.ribeirao.survey.ui.CustomViewGroup;
import br.com.ribeirao.survey.util.GsonDateFormatAdapter;
import br.com.ribeirao.survey.ws.WebService;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    private LinearLayout llContainer;

    private ProgressBar surveyProgress;
    private ImageButton previous;
    private ImageButton next;
    private Button save;

    private TextView surveyFinish;
    private LinearLayout llSurveyFinish;

    private LinearLayout llSurveyName;
    private EditText edtSurveyName;

    private MainActivity mainActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager = (ViewPager) findViewById(R.id.container);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                changeVisibilityButtons();
            }
        });

        llContainer = (LinearLayout) findViewById(R.id.llContainer);

        previous = (ImageButton) findViewById(R.id.previous);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
                changeVisibilityButtons();
            }
        });

        next = (ImageButton) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
                changeVisibilityButtons();
            }
        });

        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SaveSurveyTask(mainActivity, getApplicationContext()).execute();
            }
        });

        surveyProgress = (ProgressBar) findViewById(R.id.surveyProgress);
        surveyFinish = (TextView) findViewById(R.id.surveyFinish);
        llSurveyFinish = (LinearLayout) findViewById(R.id.llSurveyFinish);
        Button surveyClose = (Button) findViewById(R.id.surveyClose);
        surveyClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.finish();
            }
        });

        llSurveyName = (LinearLayout) findViewById(R.id.llSurveyName);
        edtSurveyName = (EditText) findViewById(R.id.edtSurveyName);
        edtSurveyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    ((InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edtSurveyName.getWindowToken(), 0);
                }
            }
        });
        Button btnSurveyName = (Button) findViewById(R.id.btnSurveyName);
        btnSurveyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llSurveyName.setVisibility(View.GONE);
                showQuestions(true);
                changeVisibilityButtons();
            }
        });

        showQuestions(false);

        new GetSurveyTask(this).execute();
    }

    private void changeVisibilityButtons() {
        int position = mViewPager.getCurrentItem();
        if (position == 0) {
            previous.setVisibility(View.INVISIBLE);
        } else {
            previous.setVisibility(View.VISIBLE);
        }
        if (position == mSectionsPagerAdapter.getSurvey().getQuestions().size() - 1) {
            save.setVisibility(View.VISIBLE);
            next.setVisibility(View.INVISIBLE);
        } else {
            next.setVisibility(View.VISIBLE);
            save.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void showQuestions(boolean show) {
        if (show) {
            surveyProgress.setVisibility(View.GONE);
            llContainer.setVisibility(View.VISIBLE);
            mViewPager.setVisibility(View.VISIBLE);
        } else {
            surveyProgress.setVisibility(View.VISIBLE);
            llContainer.setVisibility(View.GONE);
            mViewPager.setVisibility(View.GONE);
        }
    }

    public static class PlaceholderFragment extends Fragment {

        private Question question;

        private int nQuestion;

        private Map<Long, String> answerMap;

        public PlaceholderFragment() {
        }

        public void setQuestion(Question question) {
            this.question = question;
        }

        public void setnQuestion(int nQuestion) {
            this.nQuestion = nQuestion;
        }

        public void setAnswerMap(Map<Long, String> answerMap) {
            this.answerMap = answerMap;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView tvQuestion = (TextView) rootView.findViewById(R.id.question);
            tvQuestion.setText(question.getStatement());
            tvQuestion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startPlayerView(getActivity(), question.getAnimation());
                }
            });
            TextView tvnQuestion = (TextView) rootView.findViewById(R.id.nQuestion);
            tvnQuestion.setText(getString(R.string.n_question, nQuestion, getArguments().getInt("n_question")));

            populateRadioButton(rootView);

            return rootView;
        }

        public void populateRadioButton(View rootView) {
            CustomViewGroup radioGroup = (CustomViewGroup) rootView.findViewById(R.id.llOptions);
            if (question.getOptionA() != null)
                radioGroup.addOption(question.getOptionA());
            if (question.getOptionB() != null)
                radioGroup.addOption(question.getOptionB());
            if (question.getOptionC() != null)
                radioGroup.addOption(question.getOptionC());
            if (question.getOptionD() != null)
                radioGroup.addOption(question.getOptionD());
            if (question.getOptionE() != null)
                radioGroup.addOption(question.getOptionE());
            radioGroup.setListener(new CustomViewGroup.OnClickListener() {
                @Override
                public void onRadioClick(int position) {
                    switch (position) {
                        case 0:
                            answerMap.put(question.getId(), "A");
                            break;
                        case 1:
                            answerMap.put(question.getId(), "B");
                            break;
                        case 2:
                            answerMap.put(question.getId(), "C");
                            break;
                        case 3:
                            answerMap.put(question.getId(), "D");
                            break;
                        case 4:
                            answerMap.put(question.getId(), "E");
                            break;
                    }
                }

                @Override
                public void onLabelClick(int position) {
                    switch (position) {
                        case 0:
                            startPlayerView(getActivity(), question.getAnimationA());
                            break;
                        case 1:
                            startPlayerView(getActivity(), question.getAnimationB());
                            break;
                        case 2:
                            startPlayerView(getActivity(), question.getAnimationC());
                            break;
                        case 3:
                            startPlayerView(getActivity(), question.getAnimationD());
                            break;
                        case 4:
                            startPlayerView(getActivity(), question.getAnimationE());
                            break;
                    }
                }
            });
        }

        private void startPlayerView(Activity activity, String animation) {
            Intent intent = new Intent(activity, PlayerViewActivity.class);
            Bundle b = new Bundle();
            b.putString("videoId", animation);
            intent.putExtras(b);
            activity.startActivity(intent);
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private Survey survey;

        private Map<Long, String> answerMap;

        public Survey getSurvey() {
            return survey;
        }

        private Map<Long, String> getAnswerMap() {
            return answerMap;
        }

        private SectionsPagerAdapter(FragmentManager fm, Survey survey) {
            super(fm);
            this.survey = survey;
            answerMap = new HashMap<>();
        }

        @Override
        public Fragment getItem(int position) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            fragment.setnQuestion(position + 1);
            fragment.setQuestion(survey.getQuestions().get(position));
            fragment.setAnswerMap(answerMap);
            Bundle args = new Bundle();
            args.putInt("n_question", survey.getQuestions().size());
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return survey.getQuestions().size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public void startUpdate(ViewGroup container) {
            super.startUpdate(container);
        }
    }

    class GetSurveyTask extends AsyncTask<Void, Void, String> {

        private Context context;

        private GetSurveyTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Void... params) {
            String json = null;
            try {
                json = WebService.getSurvey();
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return json;
        }

        @Override
        protected void onPostExecute(final String json) {
            if (json != null) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonDateFormatAdapter(context)).create();
                Survey survey = gson.fromJson(json, Survey.class);
                if (survey != null && survey.getQuestions() != null && !survey.getQuestions().isEmpty()) {
                    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), survey);
                    mViewPager.setAdapter(mSectionsPagerAdapter);
                    llSurveyName.setVisibility(View.VISIBLE);
                    surveyProgress.setVisibility(View.GONE);
                } else {
                    surveyProgress.setVisibility(View.GONE);
                    surveyFinish.setText("Não há questionário ativo. Por favor, volte mais tarde.");
                    llSurveyFinish.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    class SaveSurveyTask extends AsyncTask<Void, Void, String> {

        private Context context;
        private Activity activity;

        private Answer answer;

        private SaveSurveyTask(Activity activity, Context context) {
            this.context = context;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            answer = new Answer();
            answer.setSurveyId(mSectionsPagerAdapter.getSurvey().getId());
            answer.setName(edtSurveyName.getText().toString());
            List<QuestionAnswer> questionAnswers = new ArrayList<>();
            for (Map.Entry<Long, String> answer : mSectionsPagerAdapter.getAnswerMap().entrySet()) {
                QuestionAnswer questionAnswer = new QuestionAnswer();
                questionAnswer.setQuestionId(answer.getKey());
                questionAnswer.setAnswer(answer.getValue());
                questionAnswers.add(questionAnswer);
            }
            answer.setQuestionAnswers(questionAnswers);
            showQuestions(false);
        }

        @Override
        protected String doInBackground(Void... params) {
            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonDateFormatAdapter(context)).create();
            String json = gson.toJson(answer, Answer.class);
            String result = null;
            try {
                result = WebService.saveSurvey(json);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null && result.equals("true")) {
                surveyProgress.setVisibility(View.GONE);
                llSurveyFinish.setVisibility(View.VISIBLE);
            } else {
                showQuestions(true);
                changeVisibilityButtons();
                Toast.makeText(activity, "Erro ao enviar as respostas! Tente novamente. Caso o erro persista, entre em contato com o administrador", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
