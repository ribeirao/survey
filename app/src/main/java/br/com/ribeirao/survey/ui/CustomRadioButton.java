package br.com.ribeirao.survey.ui;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import br.com.ribeirao.survey.R;

/**
 * Created by rafael on 06/11/17.
 */

public class CustomRadioButton extends FrameLayout {

    private OnClickListener listener;
    private TextView textView;
    private RadioButton radioButton;
    private int position = -1;


    public CustomRadioButton(@NonNull Context context) {
        super(context);
        init();
    }

    public CustomRadioButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRadioButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomRadioButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.custom_radio_button, null);
        textView = (TextView) view.findViewById(R.id.textView);
        radioButton = (RadioButton) view.findViewById(R.id.radioButton);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLabelClick(position);
            }
        });

        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onRadioClick(position);
            }
        });

        addView(view);
    }


    /*
        Util
     */
    public void setChecked(boolean checked) {
        radioButton.setChecked(checked);
    }

    public void setText(String text) {
        textView.setText(text);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setListener(OnClickListener listener) {
        this.listener = listener;
    }


    /*
        Custom listener
     */
    interface OnClickListener {

        void onRadioClick(int position);

        void onLabelClick(int position);

    }

}
