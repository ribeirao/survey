package br.com.ribeirao.survey.ui;

import android.content.Context;
import android.os.Build;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import br.com.ribeirao.survey.R;

/**
 * Created by rafael on 06/11/17.
 */

public class CustomViewGroup extends LinearLayout implements CustomRadioButton.OnClickListener {

    private OnClickListener listener;
    private ArrayList<CustomRadioButton> radioButtons;
    private int position = -1;

    public CustomViewGroup(Context context) {
        super(context);
        init();
    }

    public CustomViewGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomViewGroup(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        setOrientation(VERTICAL);
        radioButtons = new ArrayList<>();
    }

    public void addOption(String text) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.custom_radio_item, null);

        CustomRadioButton customRadioButton = (CustomRadioButton) view.findViewById(R.id.custom_radio_item);
        customRadioButton.setText(text);
        customRadioButton.setChecked(false);
        customRadioButton.setPosition(++position);
        customRadioButton.setListener(this);

        radioButtons.add(customRadioButton);
        addView(view);
    }

    public void setListener(OnClickListener listener) {
        this.listener = listener;
    }

    /*
        Events
     */
    @Override
    public void onRadioClick(int position) {
        for (CustomRadioButton customRadioButton : radioButtons) {
            customRadioButton.setChecked(false);
        }
        radioButtons.get(position).setChecked(true);
        listener.onRadioClick(position);
    }

    @Override
    public void onLabelClick(int position) {
        listener.onLabelClick(position);
    }


    /*
        Custom listener
     */
    public interface OnClickListener {

        void onRadioClick(int position);

        void onLabelClick(int position);

    }

}
