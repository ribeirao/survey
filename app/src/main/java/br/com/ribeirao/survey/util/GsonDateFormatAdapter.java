package br.com.ribeirao.survey.util;

import android.content.Context;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GsonDateFormatAdapter implements JsonSerializer<Date>,JsonDeserializer<Date> {

        private final DateFormat dateFormat;

        public GsonDateFormatAdapter(Context context) {
            Locale current = context.getResources().getConfiguration().locale;
            String f = "";
            if (current.getLanguage().equalsIgnoreCase("pt") || current.getLanguage().equalsIgnoreCase("pt_BR")) {
                f = "dd/MM/yyyy HH:mm";
            } else if (current.getLanguage().equalsIgnoreCase("en") || current.getLanguage().equalsIgnoreCase("en_US")) {
                f = "MM/dd/yyyy HH:mm";
            } else {
                f = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            }
            dateFormat = new SimpleDateFormat(f, current);
        }

        @Override
        public synchronized JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(dateFormat.format(date));
        }

        @Override
        public synchronized Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
            Timestamp stamp = new Timestamp(jsonElement.getAsLong());
            return new Date(stamp.getTime());
        }
}
