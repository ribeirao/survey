package br.com.ribeirao.survey.model;

import java.util.Date;
import java.util.List;

public class Survey {

    private Long id;
    private String name;
    private Boolean active;
    private Date creationDate;
    private Date updateDate;
    private Date closeDate;
    List<Question> questions;

    public Survey(Long id, String name, Boolean active, Date creationDate, Date updateDate, Date closeDate, List<Question> questions) {
        this.id = id;
        this.name = name;
        this.active = active;
        this.creationDate = creationDate;
        this.updateDate = updateDate;
        this.closeDate = closeDate;
        this.questions = questions;
    }

    public Survey() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", creationDate=" + creationDate +
                ", updateDate=" + updateDate +
                ", closeDate=" + closeDate +
                ", questions=" + questions +
                '}';
    }
}
