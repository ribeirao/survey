package br.com.ribeirao.survey.model;

import java.util.List;

public class Answer {

    private Long surveyId;
    private String name;
    private List<QuestionAnswer> questionAnswers;

    public Long getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    public List<QuestionAnswer> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(List<QuestionAnswer> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }

    public Answer(Long surveyId, String name, List<QuestionAnswer> questionAnswers) {
        this.surveyId = surveyId;
        this.name = name;
        this.questionAnswers = questionAnswers;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Answer() {
    }

    @Override
    public String toString() {
        return "Answer{" +
                "surveyId=" + surveyId +
                ", questionAnswers=" + questionAnswers +
                '}';
    }
}
