package br.com.ribeirao.survey.model;

public class QuestionAnswer {

    private Long questionId;
    private String answer;

    public QuestionAnswer(Long questionId, String answer) {
        this.questionId = questionId;
        this.answer = answer;
    }

    public QuestionAnswer() {
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "QuestionAnswer{" +
                "questionId=" + questionId +
                ", answer='" + answer + '\'' +
                '}';
    }
}
