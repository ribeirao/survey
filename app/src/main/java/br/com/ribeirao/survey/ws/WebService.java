package br.com.ribeirao.survey.ws;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

public class WebService {

    public static String getSurvey() throws IOException, JSONException {
        String urlString = "https://survey-web.herokuapp.com/surveys";
        StringBuilder builder = new StringBuilder();
        URL url = new URL(urlString);
        Authenticator.setDefault(new Authenticator(){
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("survey","mudar@123".toCharArray());
            }});
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setUseCaches(false);
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        builder.append(reader.readLine());
        urlConnection.disconnect();
        return builder.toString();
    }

    public static String saveSurvey(String json) throws IOException, JSONException {
        String urlString = "https://survey-web.herokuapp.com/surveys";
        URL url = new URL(urlString);
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("survey", "mudar@123".toCharArray());
            }
        });
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(30000);
        urlConnection.setReadTimeout(30000);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/json");
        urlConnection.setDoOutput(true);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
        bw.write(json);
        bw.flush();
        bw.close();
        StringBuilder builder = new StringBuilder();
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        builder.append(reader.readLine());
        urlConnection.disconnect();
        return builder.toString();
    }

}
